/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global log */
/* eslint no-underscore-dangle: warn  */
/* eslint no-loop-func: warn */
/* eslint no-cond-assign: warn */
/* eslint no-unused-vars: warn */
/* eslint consistent-return: warn */

/* Required libraries.  */
const fs = require('fs-extra');
const path = require('path');
const uuid = require('uuid');

/* The schema validator */
const AjvCl = require('ajv');

/* Fetch in the other needed components for the this Adaptor */
const EventEmitterCl = require('events').EventEmitter;

const { MongoClient } = require('mongodb');

let myid = null;
let errors = [];

/**
 * @summary Build a standard error object from the data provided
 *
 * @function formatErrorObject
 * @param {String} origin - the originator of the error (optional).
 * @param {String} type - the internal error type (optional).
 * @param {String} variables - the variables to put into the error message (optional).
 * @param {Integer} sysCode - the error code from the other system (optional).
 * @param {Object} sysRes - the raw response from the other system (optional).
 * @param {Exception} stack - any available stack trace from the issue (optional).
 *
 * @return {Object} - the error object, null if missing pertinent information
 */
function formatErrorObject(origin, type, variables, sysCode, sysRes, stack) {
  log.trace(`${myid}-adapter-formatErrorObject`);

  // add the required fields
  const errorObject = {
    icode: 'AD.999',
    IAPerror: {
      origin: `${myid}-unidentified`,
      displayString: 'error not provided',
      recommendation: 'report this issue to the adapter team!'
    }
  };

  if (origin) {
    errorObject.IAPerror.origin = origin;
  }
  if (type) {
    errorObject.IAPerror.displayString = type;
  }

  // add the messages from the error.json
  for (let e = 0; e < errors.length; e += 1) {
    if (errors[e].key === type) {
      errorObject.icode = errors[e].icode;
      errorObject.IAPerror.displayString = errors[e].displayString;
      errorObject.IAPerror.recommendation = errors[e].recommendation;
    } else if (errors[e].icode === type) {
      errorObject.icode = errors[e].icode;
      errorObject.IAPerror.displayString = errors[e].displayString;
      errorObject.IAPerror.recommendation = errors[e].recommendation;
    }
  }

  // replace the variables
  let varCnt = 0;
  while (errorObject.IAPerror.displayString.indexOf('$VARIABLE$') >= 0) {
    let curVar = '';

    // get the current variable
    if (variables && Array.isArray(variables) && variables.length >= varCnt + 1) {
      curVar = variables[varCnt];
    }
    varCnt += 1;
    errorObject.IAPerror.displayString = errorObject.IAPerror.displayString.replace('$VARIABLE$', curVar);
  }

  // add all of the optional fields
  if (sysCode) {
    errorObject.IAPerror.code = sysCode;
  }
  if (sysRes) {
    errorObject.IAPerror.raw_response = sysRes;
  }
  if (stack) {
    errorObject.IAPerror.stack = stack;
  }

  // return the object
  return errorObject;
}

/**
 * @summary Checks for dates that have been stringified (in transport) and converts them back to dates
 *
 * @function convertDatesBack
 * @param {AnyData} data - the object to check for dates within (required).
 *
 * @return {Object} - the object containing dates
 */
function convertDatesBack(data) {
  log.trace(`${myid}-adapter-convertDatesBack`);

  // if no data - return it
  if (data === undefined || data === null) {
    return data;
  }

  // if it is a number or boolean - just return it
  if (typeof data === 'number' || typeof data === 'boolean') {
    return data;
  }

  // if it is a string need to check if a date string
  if (typeof data === 'string') {
    // if string check to see if in data format
    const dateRegex = RegExp(/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])T(0[0-9]|1[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9]).([0-9][0-9][0-9])Z$/);

    if (dateRegex.test(data)) {
      // return the date instead of the string
      return new Date(data);
    }

    return data;
  }

  // if the data is a regular expression - just return it
  if (typeof data === 'object' && data.constructor === RegExp) {
    return data;
  }

  // if data is an object but not an array
  if (typeof data === 'object' && !Array.isArray(data)) {
    // get all of the keys from the object
    const keys = Object.keys(data);
    const converted = {};

    // go through the keys to handle the individual fields
    for (let k = 0; k < keys.length; k += 1) {
      converted[[keys[k]]] = convertDatesBack(data[keys[k]]);
    }

    return converted;
  }

  // must be an array
  const converted = [];
  for (let a = 0; a < data.length; a += 1) {
    converted.push(convertDatesBack(data[a]));
  }

  return converted;
}

/**
 * This is the adapter/interface into Mongo
 */
class DBMongo extends EventEmitterCl {
  /**
   * DBMongo Adapter
   * @constructor
   */
  constructor(prongid, properties) {
    super();
    this.alive = false;
    this.id = prongid;
    myid = prongid;

    // set up the properties I care about
    this.refreshProperties(properties);

    // get the path for the specific error file
    const errorFile = path.join(__dirname, '/error.json');

    // if the file does not exist - error
    if (!fs.existsSync(errorFile)) {
      const origin = `${this.id}-adapter-constructor`;
      log.warn(`${origin}: Could not locate ${errorFile} - errors will be missing details`);
    }

    // Read the action from the file system
    const errorData = JSON.parse(fs.readFileSync(errorFile, 'utf-8'));
    ({ errors } = errorData);
  }

  /**
   * refreshProperties is used to set up all of the properties for the adapter.
   * It allows properties to be changed later by simply calling refreshProperties rather
   * than having to restart the adapter.
   *
   * @function refreshProperties
   * @param {Object} properties - an object containing all of the properties
   */
  refreshProperties(properties) {
    const meth = 'adapter-refreshProperties';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      // Read the properties schema from the file system
      const propertiesSchema = JSON.parse(fs.readFileSync(path.join(__dirname, 'propertiesSchema.json'), 'utf-8'));

      // validate the entity against the schema
      const ajvInst = new AjvCl();
      const validate = ajvInst.compile(propertiesSchema);
      const result = validate(properties);

      // if invalid properties and did not already have properties, stop
      if (!result && !this.props) {
        log.error(`Attempt to configure adapter with invalid properties - ${JSON.stringify(validate.errors)}`);
        this.alive = false;
        this.emit('OFFLINE', { id: this.id });
        return;
      }

      // if invalid properties but had valid ones, keep the valid ones
      if (!result) {
        log.warn('Attempt to configure adapter with invalid properties!');
        return;
      }

      // for now just set this.props - may do individual properties later
      this.props = properties;
    } catch (e) {
      log.error(`${origin}: Properties may not have been set properly. ${e}`);
    }
  }

  /**
   * Call to connect and authenticate to the database
   *
   * @function connect
   */
  connect() {
    const meth = 'adapter-connect';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    // if there is no url and no host then we need to error
    if (!this.props.host && !this.props.url) {
      log.error('ERROR: Missing required host or url - can not connect to database');
      this.alive = false;
      this.emit('OFFLINE', { id: this.id });
      return;
    }

    // set up the default variables that will be used
    let url = '';
    let { db } = this.props;
    let dbProtocol = 'mongodb';
    let dbAuthEnabled = false;
    let dbUsername = null;
    let dbPassword = null;
    let dbAuthString = null;
    let hostString = `${this.props.host}:27017`;
    let replSetEnabled = false;
    let replSet = null;

    // if there is a port defined change the hostString to include it
    if (this.props.port) {
      hostString += `${this.props.host}:${this.props.port}`;
    }

    // get the authentication properties if provided
    if (this.props.credentials) {
      if (this.props.credentials.dbAuth === true) {
        dbAuthEnabled = true;
      }
      if (this.props.credentials.user) {
        dbUsername = this.props.credentials.user;
      }
      if (this.props.credentials.passwd) {
        dbPassword = this.props.credentials.passwd;
      }
    }

    // get the replica set properties if provided
    if (this.props.replSet) {
      if (this.props.replSet.enabled === true) {
        replSetEnabled = true;
      }
      if (this.props.replSet.replicaSet) {
        replSet = this.props.replSet.replicaSet;
      }
    }

    // if a url has been provided, pull it a part. URL overrides the individual properties!
    if (this.props.url && this.props.url.length > 0) {
      ({ url } = this.props);

      // Handles some possible errors on the url
      if (url.indexOf('//') < 0) {
        log.warn('WARNING: URL missing protocol (e.g. mongodb://) may not parse properly!');
      }
      if (url.indexOf(' ') >= 0) {
        log.warn('WARNING: The provided URL contains spaces and may not parse properly!');
      }

      // get the main indexes that we need from the url
      let hostIndex = url.indexOf('://');

      // split the string so we can get all the pieces that are in the url
      // if no // assume that the mongodb:// was not there
      if (hostIndex < 0) {
        hostIndex = 0;
        hostString = url;
      } else {
        dbProtocol = url.substring(0, hostIndex);
        hostString = url.substring(hostIndex + 3);
      }

      let replIndex = hostString.indexOf('/?');

      // just in case they forgot the / after the port or if they defined the database
      if (replIndex < 0) {
        replIndex = hostString.indexOf('?');
      }

      // if there is a replica set, strip it out of the url
      if (replIndex >= 0) {
        replSet = hostString.substring(replIndex + 1);
        // remove the starting ? if there
        if (replSet.indexOf('?') === 0) {
          replSet = replSet.substring(1);
        }
        hostString = hostString.substring(0, replIndex);
        replSetEnabled = true;
      }

      // if there is credentials strip them out of the hostString
      const credIndex = hostString.lastIndexOf('@');
      if (credIndex >= 0) {
        dbAuthString = hostString.substring(0, credIndex);
        hostString = hostString.substring(credIndex + 1);
        dbAuthEnabled = true;
      }

      // if there is a database on the url take it out
      const dbIndex = hostString.indexOf('/');
      if (dbIndex >= 0) {
        db = hostString.substring(dbIndex + 1);
        hostString = hostString.substring(0, dbIndex);
      }
    }

    // if no db throw a warning but then use test as the database
    if (!db) {
      log.warn('WARNING: No database found in property or url - connecting to test.');
      db = 'test';
    }
    this.database = db;

    // format the url based on authentication or no authentication
    if (!dbAuthEnabled) {
      // format the url
      url = `${dbProtocol}://${hostString}`;
    } else {
      if (!dbAuthString) {
        // if authenticating but no credentials error out
        if (!dbUsername || !dbPassword) {
          log.error('ERROR: Database authentication is configured but username or password is not provided');
          this.alive = false;
          this.emit('OFFLINE', { id: this.id });
          return;
        }

        // format the dbAuthString - urlencoding the username and password
        dbAuthString = `${encodeURIComponent(dbUsername)}:${encodeURIComponent(dbPassword)}`;
      }

      // format the url
      url = `${dbProtocol}://${dbAuthString}@${hostString}/${db}`;
    }

    // are we using a replication set need to add it to the url
    if (replSetEnabled && replSet) {
      url += `?${replSet}`;
    }

    // define some local variables to help in validating the properties.json file
    let sslEnabled = false;
    let sslValidate = false;
    let sslCheckServerIdentity = false;
    let sslCA = null;

    /*
      * this first section is configuration mapping
      * it can be replaced with the config object when available
      */
    if (this.props.ssl) {
      // enable ssl encryption?
      if (this.props.ssl.enabled === true) {
        log.info('Connecting to MongoDB with SSL.');
        sslEnabled = true;
        // validate the server's certificate
        // against a known certificate authority?
        if (this.props.ssl.acceptInvalidCerts === false) {
          sslValidate = true;
          log.info('Certificate based SSL MongoDB connections will be used.');
          // if validation is enabled, we need to read the CA file
          if (this.props.ssl.sslCA) {
            try {
              sslCA = [fs.readFileSync(this.props.ssl.sslCA)];
            } catch (err) {
              log.error(`Error: Unable to load Mongo CA file: ${err}`);
              this.alive = false;
              this.emit('OFFLINE', {
                id: this.id
              });
              return;
            }
          } else {
            log.error('Error: Certificate validation'
              + 'is enabled but a CA is not specified.');
            this.alive = false;
            this.emit('OFFLINE', {
              id: this.id
            });
            return;
          }
        } else {
          log.info('SSL MongoDB connection without CA certificate validation.');
        }
        // validate the server certificate against the configured url?
        if (this.props.ssl.checkServerIdentity === true) {
          sslCheckServerIdentity = true;
        } else {
          log.warn('WARNING: Skipping server identity validation');
        }
      } else {
        log.warn('WARNING: Connecting to MongoDB without SSL.');
      }
    } else {
      log.warn('WARNING: Connecting to MongoDB without SSL.');
    }

    // This second section is to construct the mongo options object
    const options = {
      reconnectTries: 2000,
      reconnectInterval: 1000,
      ssl: sslEnabled,
      sslValidate,
      checkServerIdentity: sslCheckServerIdentity
    };
    if (sslValidate === true) {
      options.sslCA = sslCA;
    }
    // const options = (replSetEnabled === true) ? { replSet: opts } : { server: opts };
    log.debug(`Connecting to MongoDB with url ${url}`);
    log.debug(`Connecting to MongoDB with options ${JSON.stringify(options)}`);

    // Now we will start the process of connecting to mongo db
    MongoClient.connect(url, options, (err, mongoClient) => {
      if (!mongoClient) {
        log.error(`Error! Exiting... Must start MongoDB first ${err}`);
        this.alive = false;
        this.emit('OFFLINE', {
          id: this.id
        });
      } else {
        log.info(`mongo running @${url}/${db}`);
        this.clientDB = mongoClient.db(db);

        mongoClient.on('close', () => {
          this.alive = false;
          this.emit('OFFLINE', {
            id: this.id
          });
          log.error('MONGO CONNECTION LOST...');
        });

        mongoClient.on('reconnect', () => {
          // we still need to check if we are properly authenticated
          // so we just list collections to test it.
          this.clientDB.collections((error) => {
            if (error) {
              log.error(error);
              this.alive = false;
              this.emit('OFFLINE', {
                id: this.id
              });
            } else {
              log.info('MONGO CONNECTION BACK...');
              this.alive = true;
              this.emit('ONLINE', {
                id: this.id
              });
            }
          });
        });

        // we still need to check if we are properly authenticated
        // so we just list collections to test it.
        this.clientDB.collections((error) => {
          if (error) {
            log.error(error);
            this.alive = false;
            this.emit('OFFLINE', {
              id: this.id
            });
          } else {
            /*
            * once we are connected to mongo, we need to perform a health check
            * to ensure we can read from the database
            */
            log.info('MONGO CONNECTION UP...');
            this.alive = true;
            this.emit('ONLINE', {
              id: this.id
            });
            log.info('Successfully authenticated with the Mongo DB server');
            this.healthCheck((hc) => {
              if (hc.status === 'fail') {
                log.error('Error connecting to Mongo DB:'
                  + 'health check has failed.');
                this.alive = false;
                this.emit('OFFLINE', {
                  id: this.id
                });
              } else {
                // successful health check; now we
                // can declare the database as online
                log.info('MongoDB connection has been established');
                this.alive = true;
                this.emit('ONLINE', {
                  id: this.id
                });
              }
            });
          }
        });
      }
    });
  }

  /**
   * Call to run a healthcheck on the database
   *
   * @function healthCheck
   * @param {healthCallback} callback - a callback function to return a result
   *                                    healthcheck success or failure
   */
  healthCheck(callback) {
    const meth = 'adapter-healthCheck';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      // verify that we are connected to Mongo
      if (!this.alive || !this.clientDB) {
        log.error('Error during healthcheck: Not connected to Database');
        return callback({
          id: this.id,
          status: 'fail'
        });
      }

      return this.clientDB.stats((err) => {
        if (err) {
          log.error(`Error during healthcheck: ${err}`);
          return callback({
            id: this.id,
            status: 'fail'
          });
        }

        return callback({
          id: this.id,
          status: 'success'
        });
      });
    } catch (ex) {
      log.error(`Exception during healthcheck: ${ex}`);
      return callback({
        id: this.id,
        status: 'fail'
      });
    }
  }

  /**
   * getAllFunctions is used to get all of the exposed function in the adapter
   *
   * @function getAllFunctions
   */
  getAllFunctions() {
    let myfunctions = [];
    let obj = this;

    // find the functions in this class
    do {
      const l = Object.getOwnPropertyNames(obj)
        .concat(Object.getOwnPropertySymbols(obj).map((s) => s.toString()))
        .sort()
        .filter((p, i, arr) => typeof obj[p] === 'function' && p !== 'constructor' && (i === 0 || p !== arr[i - 1]) && myfunctions.indexOf(p) === -1);
      myfunctions = myfunctions.concat(l);
    }
    while (
      (obj = Object.getPrototypeOf(obj)) && Object.getPrototypeOf(obj)
    );

    return myfunctions;
  }

  /**
   * getWorkflowFunctions is used to get all of the workflow function in the adapter
   *
   * @function getWorkflowFunctions
   */
  getWorkflowFunctions() {
    const myfunctions = this.getAllFunctions();
    const wffunctions = [];

    // remove the functions that should not be in a Workflow
    for (let m = 0; m < myfunctions.length; m += 1) {
      if (myfunctions[m] === 'addListener') {
        // got to the second tier (adapterBase)
        break;
      }
      if (myfunctions[m] !== 'connect' && myfunctions[m] !== 'healthCheck'
        && myfunctions[m] !== 'getAllFunctions' && myfunctions[m] !== 'getWorkflowFunctions'
        && myfunctions[m] !== 'refreshProperties') {
        wffunctions.push(myfunctions[m]);
      }
    }

    return wffunctions;
  }

  /**
   * Call to create an item in the database
   *
   * @function create
   * @param {String} collectionName - the collection to save the item in. (required)
   * @param {String} data - the modification to make. (required)
   * @param {createCallback} callback - a callback function to return a result
   *                                    (created item) or the error
   */
  create(collectionName, data, callback) {
    const meth = 'adapter-create';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      // verify the required data has been provided
      if (!collectionName) {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['collectionName'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      if (!data) {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['data'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // verify that we are connected to Mongo
      if (!this.alive || !this.clientDB) {
        const errorObj = formatErrorObject(origin, 'Database Error', [`${this.database} not connected`], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // get the collection so we can run the remove on the collection
      const collection = this.clientDB.collection(collectionName);

      if (!collection) {
        const errorObj = formatErrorObject(origin, 'Database Error', [`Collection ${collectionName} not found`], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const dataInfo = data;
      if (!{}.hasOwnProperty.call(dataInfo, '_id')) {
        dataInfo._id = uuid.v4();
      }

      // insert the item into the collection
      return collection.insertOne(convertDatesBack(dataInfo), {}, (err, result) => {
        if (err) {
          const errorObj = formatErrorObject(origin, 'Database Error', [err], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
        return callback({
          status: 'success',
          code: 200,
          response: result.ops[0]
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * Call to create many items in the database
   *
   * @function createMany
   * @param {String} collectionName - the collection to save the items in. (required)
   * @param {Array} data - the modification to make. (required)
   * @param {Boolean} ordered - whether the data needs to be ordered. (optional)
   * @param {String} writeConcern - whether the data should be overwritten. (optional)
   * @param {createCallback} callback - a callback function to return a result
   *                                    (created items) or the error
   */
  createMany(collectionName, data, ordered, writeConcern, callback) {
    const meth = 'adapter-createMany';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      // verify the required data has been provided
      if (!collectionName) {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['collectionName'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      if (!data) {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['data'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      if (!Array.isArray(data)) {
        const errorObj = formatErrorObject(origin, 'Invalid data format - data must be an array', null, null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // verify that we are connected to Mongo
      if (!this.alive || !this.clientDB) {
        const errorObj = formatErrorObject(origin, 'Database Error', [`${this.database} not connected`], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // get the collection so we can run the remove on the collection
      const collection = this.clientDB.collection(collectionName);

      if (!collection) {
        const errorObj = formatErrorObject(origin, 'Database Error', [`Collection ${collectionName} not found`], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const dataInfo = data;
      const options = {
        ordered,
        writeConcern
      };

      for (let i = 0; i < dataInfo.length; i += 1) {
        if (!{}.hasOwnProperty.call(dataInfo[i], '_id')) {
          dataInfo[i]._id = uuid.v4();
        }
      }

      // insert the items into the collection
      return collection.insertMany(convertDatesBack(dataInfo), options, (err, result) => {
        if (err) {
          const errorObj = formatErrorObject(origin, 'Database Error', [err], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
        return callback({
          status: 'success',
          code: 200,
          response: result.ops[0]
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * Call to query items in the database
   *
   * @function query
   * @param {Object} queryDoc - the query to use to find data. (required)
   * queryDoc = {
   *  collection : <collection_name>,
   *  filter : <filter Obj>,
   *  projection : <projection Obj>
   * }
   * @param {getCallback} callback - a callback function to return a result
   *                                 (items) or the error
   */
  query(queryDoc, callback) {
    const meth = 'adapter-query';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      if (typeof queryDoc === 'string') {
        queryDoc = JSON.parse(queryDoc);
      }
      // verify the required data has been provided
      if (!queryDoc) {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['queryDoc'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      if (!queryDoc.collection) {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['queryDoc.collection'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // verify that we are connected to Mongo
      if (!this.alive || !this.clientDB) {
        const errorObj = formatErrorObject(origin, 'Database Error', [`${this.database} not connected`], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // get the collection so we can run the remove on the collection
      const collection = this.clientDB.collection(queryDoc.collection);

      if (!collection) {
        const errorObj = formatErrorObject(origin, 'Database Error', [`Collection ${queryDoc.collection} not found`], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // find the items in the collection
      return collection.find((convertDatesBack(queryDoc.filter) || {})).project((queryDoc.projection || {})).toArray((err, docs) => {
        if (err) {
          const errorObj = formatErrorObject(origin, 'Database Error', [err], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
        return callback({
          status: 'success',
          code: 200,
          response: docs
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * Call to find items in the database
   *
   * @function find
   * @param {Object} options - the options to use to find data. (required)
   * options = {
   *  entity : <collection_name>,
   *  filter : <filter Obj>,
   *  sort : <sort Obj>,
   *  start : <start position>,
   *  limit : <limit of results>,
   * }
   * @param {deleteCallback} callback - a callback function to return a result
   *                                    (status of the request) or the error
   */
  find(options, callback) {
    const meth = 'adapter-find';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      if (typeof options === 'string') {
        options = JSON.parse(options);
      }
      // verify the required data has been provided
      if (!options) {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['options'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      if (!options.entity) {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['options.entity'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // verify that we are connected to Mongo
      if (!this.alive || !this.clientDB) {
        const errorObj = formatErrorObject(origin, 'Database Error', [`${this.database} not connected`], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // get the collection so we can run the remove on the collection
      const { entity } = options;
      const collection = this.clientDB.collection(entity);

      if (!collection) {
        const errorObj = formatErrorObject(origin, 'Database Error', [`Collection ${entity} not found`], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const start = options.start || 0;
      const filter = options.filter || {};
      const sort = options.sort || {};
      let limit;

      // If limit is not specified, default to 10.
      // Note: limit may be 0, which is equivalent to setting no limit.
      if (!Object.hasOwnProperty.call(options, 'limit')) {
        limit = 10;
      } else {
        ({ limit } = options);
      }

      // Replace filter with regex to allow for substring lookup
      // TODO: Need to create a new filter object instead of mutating the exsisting one
      const filterKeys = Object.keys(filter).filter((key) => (key[0] !== '$' && typeof filter[key] === 'string'));
      filterKeys.map((key) => {
        try {
          const escapedFilter = filter[key].replace(/([.?*+^$[\]\\(){}|-])/g, '\\$1');
          const regexedFilter = new RegExp(`.*${escapedFilter}.*`, 'i');
          filter[key] = {
            $regex: regexedFilter
          };
        } catch (e) {
          delete filter[key];
        }
        return key;
      });

      // find the items in the collection
      return collection.find(convertDatesBack(filter)).sort(sort).skip(start).limit(limit)
        .toArray((err, docs) => {
          if (err) {
            const errorObj = formatErrorObject(origin, 'Database Error', [err], null, null, null);
            log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
            return callback(null, errorObj);
          }

          return this.count(entity, filter, (docCount, error) => {
            if (error) {
              log.warn('returning results without total count');
            }
            return callback({
              status: 'success',
              code: 200,
              response: docs,
              total: docCount.response
            });
          });
        });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * Call to search for count of items in the database
   *
   * @function count
   * @param {String} collectionName - the collection to find things from. (required)
   * @param {Object} filter - the filter for items to search for. (optional)
   * @param {deleteCallback} callback - a callback function to return a result
   *                                    (status of the request) or the error
   */
  count(collectionName, filter, callback) {
    const meth = 'adapter-count';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      if (typeof filter === 'string') {
        filter = JSON.parse(filter);
      }
      // prepare the aggregation to determine count
      const aggregations = [
        {
          $match: filter || {}
        }, {
          $group: {
            _id: null,
            count: {
              $sum: 1
            }
          }
        }
      ];

      return this.aggregate(collectionName, aggregations, (res, err) => {
        if (err) {
          return callback(null, err);
        }

        if (res && res.response && res.response.length === 0) {
          return callback({
            status: 'success',
            code: 200,
            response: 0
          });
        }

        if (res && res.response && res.response[0] && res.response[0].count) {
          return callback({
            status: 'success',
            code: 200,
            response: res.response[0].count
          });
        }

        // something went wrong
        const errorObj = formatErrorObject(origin, 'Database Error', [`Could not get count ${res}`], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * Execute an aggregation framework pipeline against the collection
   *
   * @function aggregate
   * @param {String} collectionName - the collection to run aggregates on. (required)
   * @param {Object} aggregations - the filter for items to search for. (required)
   * @param {updateCallback} callback - a callback function to return a result
   *                                    (the items) or the error
   */
  aggregate(collectionName, aggregations, callback) {
    const meth = 'adapter-aggregate';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      if (typeof aggregations === 'string') {
        aggregations = JSON.parse(aggregations);
      }
      // verify the required data has been provided
      if (!collectionName) {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['collectionName'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      if (!aggregations) {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['aggregations'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // verify that we are connected to Mongo
      if (!this.alive || !this.clientDB) {
        const errorObj = formatErrorObject(origin, 'Database Error', [`${this.database} not connected`], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // get the collection so we can run the remove on the collection
      const collection = this.clientDB.collection(collectionName);

      if (!collection) {
        const errorObj = formatErrorObject(origin, 'Database Error', [`Collection ${collectionName} not found`], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // run the aggregations on the collection
      return collection.aggregate(convertDatesBack(aggregations), {}).toArray((err, docs) => {
        if (err) {
          const errorObj = formatErrorObject(origin, 'Database Error', [err], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
        return callback({
          status: 'success',
          code: 200,
          response: docs
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * Call to search for items in the database
   *
   * @function search
   * @param {String} collectionName - the collection to find things from. (required)
   * @param {Object} filter - the filter for items to search for. (optional)
   * @param {getCallback} callback - a callback function to return a result
   *                                 (items) or the error
   */
  search(collectionName, filter, callback) {
    const meth = 'adapter-search';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      if (typeof filter === 'string') {
        filter = JSON.parse(filter);
      }
      // verify the required data has been provided
      if (!collectionName) {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['collectionName'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // verify that we are connected to Mongo
      if (!this.alive || !this.clientDB) {
        const errorObj = formatErrorObject(origin, 'Database Error', [`${this.database} not connected`], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // get the collection so we can run the remove on the collection
      const collection = this.clientDB.collection(collectionName);

      if (!collection) {
        const errorObj = formatErrorObject(origin, 'Database Error', [`Collection ${collectionName} not found`], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // find the item(s) in the collection
      return collection.find((convertDatesBack(filter) || {})).toArray((err, docs) => {
        if (err) {
          const errorObj = formatErrorObject(origin, 'Database Error', [err], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
        return callback({
          status: 'success',
          code: 200,
          response: docs
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * Call to get distinct items from the collection.
   *
   * @function distinct
   * @param {String} collectionName - the collection to find things from. (required)
   * @param {String} field - the field in the object that should be distinct. (required)
   * @param {Object} query - the query for the object. (optional)
   * @param {Object} options - the options for the call. (optional)
   * @param {getCallback} callback - a callback function to return a result
   *                                 (distinct items) or the error
   */
  distinct(collectionName, field, query, options, callback) {
    const meth = 'adapter-distinct';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      if (typeof query === 'string') {
        query = JSON.parse(query);
      }
      // verify the required data has been provided
      if (!collectionName) {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['collectionName'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      if (!field) {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['field'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // verify that we are connected to Mongo
      if (!this.alive || !this.clientDB) {
        const errorObj = formatErrorObject(origin, 'Database Error', [`${this.database} not connected`], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // get the collection so we can run the remove on the collection
      const collection = this.clientDB.collection(collectionName);

      if (!collection) {
        const errorObj = formatErrorObject(origin, 'Database Error', [`Collection ${collectionName} not found`], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // find the item(s) in the collection
      return collection.distinct(field, (convertDatesBack(query) || {}), (options || {}), (err, docs) => {
        if (err) {
          const errorObj = formatErrorObject(origin, 'Database Error', [err], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
        return callback({
          status: 'success',
          code: 200,
          response: docs
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * Call to find if the item(s) exist in the database.
   *
   * @function exists
   * @param {String} collectionName - the collection to find things from. (required)
   * @param {Object} query - the query for the object to find. (optional)
   * @param {getCallback} callback - a callback function to return a result
   *                                 (status of the request) or the error
   */
  exists(collectionName, query, callback) {
    const meth = 'adapter-exists';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    // get the count to see if items exist
    return this.count(collectionName, query, (numMatches, err) => {
      if (err) {
        return callback(null, err);
      }

      // return true if 1 or more items found, else false
      return callback({
        status: 'success',
        code: 200,
        response: numMatches.response > 0
      });
    });
  }

  /**
   * Call to find items in a collection with the filter and return the provided fields.
   *
   * @function filterFields
   * @param {String} collectionName - the collection to find things from. (required)
   * @param {Object} filter - the filter used to determine objects to find. (optional)
   * @param {Object} fields - the fields to return. (optional)
   * @param {getCallback} callback - a callback function to return a result
   *                                 (the items) or the error
   */
  filterFields(collectionName, filter, fields, callback) {
    const meth = 'adapter-filterFields';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      if (typeof filter === 'string') {
        filter = JSON.parse(filter);
      }
      // verify the required data has been provided
      if (!collectionName) {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['collectionName'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // verify that we are connected to Mongo
      if (!this.alive || !this.clientDB) {
        const errorObj = formatErrorObject(origin, 'Database Error', [`${this.database} not connected`], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // get the collection so we can run the remove on the collection
      const collection = this.clientDB.collection(collectionName);

      if (!collection) {
        const errorObj = formatErrorObject(origin, 'Database Error', [`Collection ${collectionName} not found`], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // find the item(s) in the collection
      return collection.find((convertDatesBack(filter) || {})).project((fields || {})).toArray((err, docs) => {
        if (err) {
          const errorObj = formatErrorObject(origin, 'Database Error', [err], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
        return callback({
          status: 'success',
          code: 200,
          response: docs
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * Call to find items in a collection with the provided criteria.
   *
   * @function sortQuery
   * @param {Object} queryDoc - the query to run. (required)
   * queryDoc = {
   *  collectionName : <collection_name>,
   *  filter : <filter Obj>,
   *  projection : <projected fields>,
   *  cursor1 : <>,
   *  cursor1value : <>,
   *  cursor2 : <>,
   *  cursor2value : <>,
   * }
   * @param {getCallback} callback - a callback function to return a result
   *                                 (the items) or the error
   */
  sortQuery(queryDoc, callback) {
    const meth = 'adapter-sortQuery';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      if (typeof queryDoc === 'string') {
        queryDoc = JSON.parse(queryDoc);
      }
      // verify the required data has been provided
      if (!queryDoc) {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['queryDoc'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      if (!queryDoc.collectionName) {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['queryDoc.collectionName'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // verify that we are connected to Mongo
      if (!this.alive || !this.clientDB) {
        const errorObj = formatErrorObject(origin, 'Database Error', [`${this.database} not connected`], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // get the collection so we can run the remove on the collection
      const collection = this.clientDB.collection(queryDoc.collectionName);

      if (!collection) {
        const errorObj = formatErrorObject(origin, 'Database Error', [`Collection ${queryDoc.collectionName} not found`], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const {
        filter,
        projection,
        cursor1,
        cursor1value,
        cursor2,
        cursor2value
      } = queryDoc;

      // find the item(s) in the collection
      // Need to determine how the cursors play into the call
      // return collection.find(filter, projection)[cursor1](cursor1value)[cursor2](cursor2value)
      return collection.find((convertDatesBack(filter) || {})).project((projection || {})).toArray((err, docs) => {
        if (err) {
          const errorObj = formatErrorObject(origin, 'Database Error', [err], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
        return callback({
          status: 'success',
          code: 200,
          response: docs
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * Call to find the first item in a collection matching the id.
   *
   * @function searchById
   * @param {String} collectionName - the collection to search. (required)
   * @param {String} id - the id of the item to find. (required)
   * @param {getCallback} callback - a callback function to return a result
   *                                 (the item) or the error
   */
  searchById(collectionName, id, callback) {
    const meth = 'adapter-searchById';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      // verify the required data has been provided
      if (!collectionName) {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['collectionName'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      if (!id) {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['id'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // verify that we are connected to Mongo
      if (!this.alive || !this.clientDB) {
        const errorObj = formatErrorObject(origin, 'Database Error', [`${this.database} not connected`], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // get the collection so we can run the remove on the collection
      const collection = this.clientDB.collection(collectionName);

      if (!collection) {
        const errorObj = formatErrorObject(origin, 'Database Error', [`Collection ${collectionName} not found`], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // find the item in the collection
      return collection.find(({ _id: id })).limit(1).next((err, doc) => {
        if (err) {
          const errorObj = formatErrorObject(origin, 'Database Error', [err], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
        return callback({
          status: 'success',
          code: 200,
          response: doc
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * Call to save data into a collection.
   *
   * @function save
   * @param {String} collectionName - the collection to save the item in. (required)
   * @param {String} id - the id of the item to save. (required)
   * @param {Object} data - the data to be saved. (required)
   * @param {createCallback} callback - a callback function to return a result
   *                                    (saved data) or the error
   */
  save(collectionName, id, data, callback) {
    const meth = 'adapter-save';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      // verify the required data has been provided
      if (!collectionName) {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['collectionName'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      if (!id) {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['id'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      if (!data) {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['data'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // verify that we are connected to Mongo
      if (!this.alive || !this.clientDB) {
        const errorObj = formatErrorObject(origin, 'Database Error', [`${this.database} not connected`], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // get the collection so we can run the remove on the collection
      const collection = this.clientDB.collection(collectionName);

      if (!collection) {
        const errorObj = formatErrorObject(origin, 'Database Error', [`Collection ${collectionName} not found`], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      if (data._id !== id) {
        const errorObj = formatErrorObject(origin, 'Invalid ID', ['The provided id does not match what is in the data'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // insert the data into the collection
      return collection.insertOne(convertDatesBack(data), {}, (err) => {
        if (err) {
          const errorObj = formatErrorObject(origin, 'Database Error', [err], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
        return callback({
          status: 'success',
          code: 200,
          response: data
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * Call to find items in a collection and update them.
   *
   * @function updateSearched
   * @param {String} collectionName - the collection to find things from. (required)
   * @param {Object} filter - the filter used to find objects. (optional)
   * @param {Object} data - the modification to make. (required)
   * @param {Boolean} multi - option for whether to update one or many. (optional)
   * @param {Boolean} upsert - option for whether to insert new objects. (optional)
   * @param {updateCallback} callback - a callback function to return a result
   *                                    (status of the request) or the error
   */
  updateSearched(collectionName, filter, data, multi, upsert, callback) {
    const meth = 'adapter-updateSearched';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      if (typeof filter === 'string') {
        filter = JSON.parse(filter);
      }
      // verify the required data has been provided
      if (!collectionName) {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['collectionName'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      if (!data) {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['data'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // verify that we are connected to Mongo
      if (!this.alive || !this.clientDB) {
        const errorObj = formatErrorObject(origin, 'Database Error', [`${this.database} not connected`], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // get the collection so we can run the remove on the collection
      const collection = this.clientDB.collection(collectionName);

      if (!collection) {
        const errorObj = formatErrorObject(origin, 'Database Error', [`Collection ${collectionName} not found`], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // update the item(s) in the collection - call varies if one or many
      if (multi) {
        return collection.updateMany((convertDatesBack(filter) || {}), { $set: convertDatesBack(data) }, { upsert }, (err, result) => {
          if (err) {
            const errorObj = formatErrorObject(origin, 'Database Error', [err], null, null, null);
            log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
            return callback(null, errorObj);
          }
          return callback({
            status: 'success',
            code: 200,
            response: `successfully updated ${result.result.nModified} documents`
          });
        });
      }

      return collection.updateOne((convertDatesBack(filter) || {}), convertDatesBack(data), { upsert }, (err, result) => {
        if (err) {
          const errorObj = formatErrorObject(origin, 'Database Error', [err], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
        return callback({
          status: 'success',
          code: 200,
          response: `successfully updated document: ${result}`
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * Call to find an item in a collection and modify it.
   *
   * @function findAndModify
   * @param {String} collectionName - the collection to find things from. (required)
   * @param {Object} filter - the filter used to find objects. (optional)
   * @param {Array} sort - how to sort the items (first one in order will be modified). (optional)
   * @param {Object} data - the modification to make. (required)
   * @param {Boolean} multi - deprecated. (optional)
   * @param {Boolean} upsert - option for the whether to insert new objects. (optional)
   * @param {updateCallback} callback - a callback function to return a result
   *                                    (the new object) or the error
   */
  findAndModify(collectionName, filter, sort, data, multi, upsert, callback) {
    const meth = 'adapter-findAndModify';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      if (typeof filter === 'string') {
        filter = JSON.parse(filter);
      }
      // verify the required data has been provided
      if (!collectionName) {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['collectionName'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      if (!data) {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['data'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // verify that we are connected to Mongo
      if (!this.alive || !this.clientDB) {
        const errorObj = formatErrorObject(origin, 'Database Error', [`${this.database} not connected`], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // get the collection so we can run the remove on the collection
      const collection = this.clientDB.collection(collectionName);

      if (!collection) {
        const errorObj = formatErrorObject(origin, 'Database Error', [`Collection ${collectionName} not found`], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const options = {
        sort: sort || {},
        multi,
        upsert,
        returnOriginal: false
      };

      // update the item in the collection
      return collection.findOneAndUpdate((convertDatesBack(filter) || {}), { $set: convertDatesBack(data) }, options, (err, result) => {
        if (err) {
          const errorObj = formatErrorObject(origin, 'Database Error', [err], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
        return callback({
          status: 'success',
          code: 200,
          response: result.value
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * Call to reserve next available entry
   Load this schema with create task.
   {
    "name": "asntable",
    "records": [
        {
            "record": [
                false,
                false
             ]
        }
    ]
   }
   * @function reserveNext
   * @param {String} collectionName - the collection to find things from. (required)
   * @param {Object} filter - the filter used to find objects.
   * @param {String} reserveString - Any string value (required)
   * @param {callback} callback - a callback function to return a result
   *                                    (the new object) or the error
   */
  reserveNextRecord(collectionName, filter, reserveString, callback) {
    const meth = 'adapter-reserveNextRecord';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    let getIndexValue;
    let makeString;
    try {
      if (typeof filter === 'string') {
        filter = JSON.parse(filter);
      }
      // verify the required data has been provided
      if (!collectionName) {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['collectionName'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      if (!reserveString) {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['reserveString'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      this.filterFields(collectionName, filter, null, (result, err) => {
        if (err) {
          return callback(null, err);
        }
        if (!result) {
          const errorObj = formatErrorObject(origin, 'Result is empty', null, null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
        const response = result.response[0].records[0].record;
        getIndexValue = response.findIndex((element) => element === false);
        log.debug(getIndexValue);
        if (getIndexValue >= 0) {
          makeString = `records.0.record.${getIndexValue}`;
        }
        if (getIndexValue < 0) {
          const errorObj = formatErrorObject(origin, 'Record is full', null, null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
        // It can be an object or a string
        const setObj = {
          $set: {
            [makeString]: reserveString
          }
        };
        log.debug(JSON.stringify(setObj));
        return this.findAndModify(collectionName, filter, null, setObj, null, null, (resultMongo, errMongo) => {
          if (errMongo) {
            return callback(null, errMongo);
          }
          return callback({
            status: 'success',
            code: 200
          }, null);
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * Call to reserve next available entry
   Load this schema with create task.
   Request:
   {
    "name" : "vcid",
    "keyLabel" "myKeyId",
    "records" : [
        {
            "5000" : {
            "circuit_id": 12345,
            "order_id": 23222,
            "customer": "Itential",
            "description": "Testing",
            "ref_ticket": 6673
          }
        }
    ]
   }
   Response:
   {
      "status": "success",
      "code": 200,
      "myKeyId": 5005
    }
   * @function reserveNextID
   * @param {String} collectionName - the collection to find things from. (required)
   * @param {Object} filter - the filter used to find objects. (Optional)
   * @param {Object} recordInfo - Schema defined object (required)
   * @param {String} keyLabel - The name of the key (required)
   * @param {Number} start - Start of a sequence (required)
   * @param {Number} end - End of a sequence (Optional)
   * @param {callback} callback - a callback function to return a result
   *                                    (the new object) or the error
   */
  reserveNextID(collectionName, filter, recordInfo, keyLabel, start, end, callback) {
    const meth = 'adapter-reserveNextID';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);
    let idNum;
    let makeString;
    try {
      if (typeof filter === 'string') {
        filter = JSON.parse(filter);
      }
      this.filterFields(collectionName, filter, null, (result, err) => {
        if (err) {
          return callback(null, err);
        }
        if (!result) {
          const errorObj = formatErrorObject(origin, 'Result is empty', null, null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
        if (result.response[0].records.length > 0) {
          const idNumList = Object.keys(result.response[0].records[0]).map((item) => parseInt(item, 10));
          idNum = Math.max(...idNumList) + 1;
          log.debug(idNum);
          makeString = `records.0.${idNum}`;
        } else {
          idNum = parseInt(start, 10);
          makeString = `records.0.${idNum}`;
        }
        if (end) {
          log.info(end);
          log.info(idNum);
          if (parseInt(end, 10) === parseInt(idNum, 10)) {
            const errorObj = formatErrorObject(origin, 'No more ids available', null, null, null, null);
            log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
            return callback(null, errorObj);
          }
        }
        const mongoPayload = {
          recordInfo,
          [keyLabel]: idNum
        };
        const setObj = {
          $set: {
            [makeString]: mongoPayload
          }
        };
        log.info(JSON.stringify(setObj));
        return this.findAndModify(collectionName, filter, null, setObj, null, null, (resultMongo, errMongo) => {
          if (errMongo) {
            return callback(null, errMongo);
          }
          return callback({
            status: 'success',
            code: 200,
            [keyLabel]: idNum
          }, null);
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * Call to delete items in a collection based on the provided id.
   *
   * @function deleteById
   * @param {String} collectionName - the collection to delete things from. (required)
   * @param {String} id - the id of the object to delete. (required)
   * @param {deleteCallback} callback - a callback function to return a result
   *                                    (status of the request) or the error
   */
  deleteById(collectionName, id, callback) {
    const meth = 'adapter-deleteById';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      // verify the required data has been provided
      if (!collectionName) {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['collectionName'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }
      if (!id) {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['id'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // verify that we are connected to Mongo
      if (!this.alive || !this.clientDB) {
        const errorObj = formatErrorObject(origin, 'Database Error', [`${this.database} not connected`], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // get the collection so we can run the remove on the collection
      const collection = this.clientDB.collection(collectionName);

      if (!collection) {
        const errorObj = formatErrorObject(origin, 'Database Error', [`Collection ${collectionName} not found`], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // remove the item from the collection
      return collection.deleteOne({ _id: id }, {}, (err, result) => {
        if (err) {
          const errorObj = formatErrorObject(origin, 'Database Error', [err], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
        if (result.result.n === 0) {
          const errorObj = formatErrorObject(origin, 'Database Error', [`Could not find id: ${id} in collection: ${collectionName}.`], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
        return callback({
          status: 'success',
          code: 200,
          response: `Successfully deleted id: ${id}`
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * Call to delete items in a collection based on the provided filter.
   *
   * @function deleteSearched
   * @param {String} collectionName - the collection to delete things from. (required)
   * @param {Object} filter - the filter used to determine objects to delete. (optional)
   * @param {deleteCallback} callback - a callback function to return a result
   *                                    (status of the request) or the error
   */
  deleteSearched(collectionName, filter, callback) {
    const meth = 'adapter-deleteSearched';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      if (typeof filter === 'string') {
        filter = JSON.parse(filter);
      }
      // verify the required data has been provided
      if (!collectionName) {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['collectionName'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // verify that we are connected to Mongo
      if (!this.alive || !this.clientDB) {
        const errorObj = formatErrorObject(origin, 'Database Error', [`${this.database} not connected`], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // get the collection so we can run the remove on the collection
      const collection = this.clientDB.collection(collectionName);

      if (!collection) {
        const errorObj = formatErrorObject(origin, 'Database Error', [`Collection ${collectionName} not found`], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      // remove the item from the collection
      return collection.deleteMany((convertDatesBack(filter) || {}), {}, (err) => {
        if (err) {
          const errorObj = formatErrorObject(origin, 'Database Error', [err], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
        return callback({
          status: 'success',
          code: 200,
          response: 'successfully deleted document'
        });
      });

      /* POSSIBLE FUTURE IF USING A STANDARD CONNECTOR
      // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
      const reqObj = {
        filter: filter
      };

      // Make the call - identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('collection', 'remove', reqObj, false, (result, error) => {
        // if we received an error or their is no response on the results return an error
        if (error) {
          return callback(null, error);
        }
        if (!Object.hasOwnProperty.call(result, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(origin, 'Invalid Response', ['deleteMapNode'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // return the response
        return callback(result);
      });
      */
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }
}

module.exports = DBMongo;
