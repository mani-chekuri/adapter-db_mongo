Mongo Adapter
===

This adapter allows interaction with a mongodb server/cluster. For efficiency, this adapter should only be used in IAP workflow calls. Calling the adapter from Applications instead of using the npm mongodb pacakges will be less efficient!

License & Maintainers
---

### Maintained by:

Itential Product Team (<product_team@itential.com>)

Check the [changelog](CHANGELOG.md) for the latest changes.

### License

Itential, LLC proprietary
