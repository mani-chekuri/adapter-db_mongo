
## 0.3.0 [10-06-2020]

* Parse string representation of objects in method parameters

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!8

---

## 0.2.2 [02-25-2020]

* Added the  atomic property to the update request data. If sort is not passed...

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!5

---

## 0.2.1 [11-21-2019]

* Patch/date time

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!4

---

## 0.2.0 [11-14-2019]

* changes to the connect method for url and property handling

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!3

---

## 0.1.7 [10-08-2019]

* Patch/auth

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!2

---

## 0.1.6 [10-07-2019]

* Patch/auth

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!2

---

## 0.1.5 [10-03-2019]

* Added mongo services

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!1

---

## 0.1.4 [09-23-2019]

* Added mongo services

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!1

---

## 0.1.3 [08-13-2019]

* Added mongo services

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!1

---

## 0.1.2 [08-13-2019]

* Added mongo services

See merge request itentialopensource/adapters/persistence/adapter-db_mongo!1

---

## 0.1.1 [08-09-2019]

* Bug fixes and performance improvements

See commit 9802246

---
